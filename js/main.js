$(document).ready(function(){
	$('.nav-link__item').on("click",function(){
		$(this).addClass("active");
	});


	$('.partners-block').slick({
	  infinite: false,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  dots: true,
	  arrows: true,
	  responsive: [
	     {
	       breakpoint: 1024,
	       settings: {
	         slidesToShow: 3,
	         slidesToScroll: 3,
	         infinite: true,
	         dots: true
	       }
	     },
	     {
	       breakpoint: 800,
	       settings: {
	         slidesToShow: 2,
	         slidesToScroll: 2
	       }
	     },
	     {
	       breakpoint: 480,
	       settings: {
	         slidesToShow: 1,
	         slidesToScroll: 1
	       }
	     }
	   ]
	});

	$('.partners-galery').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled:true
		},
		zoom: {
	  	enabled:false,
	  	opener: function(element) {
	  		return element.find('img');
	 	 }
	  	}

	});

	var backTop =  $('.back-top')

	$(window).on('scroll',function(){
		if($(window).scrollTop() > 500) {
			backTop.fadeIn();
		} else {
			backTop.fadeOut();
		}
	})

	$('.back-top').on('click', function(){
		$('html,body').animate({scrollTop:0}, 3000);
	});

	$('#about').on('click', function(){
		$('.info').scrollTo();
	}) 
	
	$('.down').click(function(e){
		var linkHref = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(linkHref).offset().top
		}, 1000);
	});
});

function myMap() {
var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

